"use strict";

//! 1. Напишіть функцію, яка повертає частку двох чисел. Виведіть результат роботи функції в консоль.

// function division(a, b) {
//   let result = a / b;
//   return result;
// }
// console.log(division(10, 2));

//!2. Завдання: Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами.

// let num1, num2;
// while (true) {
//   num1 = prompt("Введіть перше число:");
//   num2 = prompt("Введіть друге число:");
//   if (
//     (num1 !== null && num1 !== "" && !isNaN(num1),
//     num2 !== null && num2 !== "" && !isNaN(num2))
//   ) {
//     break;
//   } else {
//     num1 = prompt("Введіть перше число:");
//     num2 = prompt("Введіть друге число:");
//   }
// }
// let operation;
// while (true) {
//   operation = prompt("Введіть операцію:");
//   if (
//     operation == "+" ||
//     operation == "-" ||
//     operation == "/" ||
//     operation == "*"
//   ) {
//     break;
//   } else {
//     alert("Такої операції не існує!");
//   }
// }
// function mathOperation() {
//   let result;
//   if (operation == "+") {
//     result = num1 + num2;
//   } else if (operation == "-") {
//     result = num1 - num2;
//   } else if (operation == "*") {
//     result = num1 * num2;
//   } else {
//     result = num1 / num2;
//   }
//   return result;
// }
// alert(`${mathOperation()}`);

//! 3. Опціонально. Завдання:
//! Реалізувати функцію підрахунку факторіалу числа.

const number = prompt("Введіть число:");

function factorial(n) {
  if (n === 0) {
    return 1;
  } else {
    return n * factorial(n - 1);
  }
}
alert(factorial(number));
